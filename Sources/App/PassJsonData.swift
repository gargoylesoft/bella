//
//  PassData.swift
//  
//
//  Created by Scott Grosch on 11/30/19.
//

import Foundation
import PassKit

struct PassJsonData: Encodable {
    public static let token = "EB80D9C6-AD37-41A0-875E-3802E88CA478"

    private let formatVersion = 1
    private let passTypeIdentifier: String = "pass.com.gargoylesoft.bella"
    private let serialNumber: String
    private let teamIdentifier: String = "BQUPLV7KWS"
    private let organizationName: String = "Bella Sorelle, LLC"
    private let description: String = "Loyalty Card"
    private let logoText: String = "Bella Sorelle Loyalty Card"
    private let foregroundColor: String = "rgb(255, 255, 255)"
    private let backgroundColor: String = "rgb(135, 129, 189)"
    private let labelColor: String = "rgb(45, 54, 129)"
    // private let webServiceURL = "http://192.168.1.39:8080/api"
    private let webServiceURL = "http://10.11.109.46:8080/api"
    private let authenticationToken = token

    private struct Barcode: Encodable {
        let message: String
        let format: String = "PKBarcodeFormatQR"
        let messageEncoding: String = "iso-8859-1"
    }

    private struct StoreCard: Encodable {
        struct Fields: Encodable {
            let key: String = "offer"
            let label: String = "Punches"
            let value: String
        }

        let primaryFields: [Fields]

        init(punches: Int) {
            primaryFields = [Fields(value: String(punches))]
        }
    }

    private let barcode: Barcode
    private let storeCard: StoreCard
    private let locations: [[String: Double]] = [
        [
            // JF4
            "altitude": 121.92,
            "latitude": 45.543611,
            "longitude": -121.037222
        ]
    ]

    init<P: PassKitPass>(data: PassData, pass: P) {
        let id = try! pass.requireID()
        self.serialNumber = "\(id)"
        barcode = Barcode(message: "https://www.gargoylesoft.com/b/update.php?uuid=\(serialNumber)")
        storeCard = StoreCard(punches: data.punches)
    }
}
