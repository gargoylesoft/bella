//
//  Pass.swift
//
//  Created by Scott Grosch on 11/21/19.
//

import Vapor
import Fluent
import PassKit
import FluentPostgresDriver

public class PassData: PassKitPassData {
    public static var schema = "pass_data"
    
    @ID(key: "id")
    public var id: Int?
    
    @Parent(key: "pass_id")
    public var pass: PKPass
    
    @Field(key: "punches")
    public var punches: Int
    
    public required init() {}
}

extension PassData: Migration {
    public func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema(Self.schema)
            .field("id", .int, .identifier(auto: true))
            .field("punches", .int, .required)
            .field("pass_id", .uuid, .required)
            .foreignKey("pass_id", references: PKPass.schema, "id", onDelete: .cascade)
            .create()
            .flatMap {
                guard let db = database as? PostgresDatabase else {
                    fatalError("Looks like you're not using PostgreSQL any longer!")
                }
                
                return .andAllSucceed(
                    trigger.map { db.sql().raw($0).run() },
                    on: db.eventLoop
                )
        }
    }
    
    public func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema(Self.schema).delete()
    }
}

// db.sql().raw() doesn't allow for multiple statements, so make it an array
private let trigger: [SQLQueryString] = [
    """
    CREATE OR REPLACE FUNCTION "public"."UpdateModified"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE \(PKPass.schema)
    SET modified = now()
    WHERE "id" = NEW.pass_id;
    
    RETURN NEW;
    END;
    $$;
    """,
    
    """
    DROP TRIGGER IF EXISTS "OnPassDataUpdated" ON "public"."\(PassData.schema)";
    """,
    
    """
    CREATE TRIGGER "OnPassDataUpdated"
    AFTER UPDATE OF "punches" ON "public"."\(PassData.schema)"
    FOR EACH ROW
    EXECUTE PROCEDURE "public"."UpdateModified"();
    """
]
