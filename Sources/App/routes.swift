import Fluent
import Vapor
import PassKit

let delegate = PKD()
var logger = Logger(label: "com.gargoylesoft.bella")

func routes(_ app: Application) throws {
    logger.logLevel = .debug

    let pk = PassKit(app: app, delegate: delegate, logger: logger)
    pk.registerRoutes(authorizationCode: PassJsonData.token)

    try pk.registerPushRoutes(middleware: PushAuthMiddleware())

    app.get("foo") { (req) -> EventLoopFuture<String> in
        PassData.find(1, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { pass in
                pass.punches += 1

                return pass.update(on: req.db).map { return "Done" }
        }
    }
}
