//
//  File.swift
//  
//
//  Created by Scott Grosch on 12/21/19.
//

import Vapor
import Fluent
import PassKit

class PKD: PassKitDelegate {
    func encode<P: PassKitPass>(pass: P, db: Database, encoder: JSONEncoder) -> EventLoopFuture<Data> {
        return PassData.query(on: db)
            .filter(\.$pass == pass.id!)
            .first()
            .unwrap(or: Abort(.internalServerError))
            .flatMap { passData in
                guard let data = try? encoder.encode(PassJsonData(data: passData, pass: pass)) else {
                    return db.eventLoop.makeFailedFuture(Abort(.internalServerError))
                }
                return db.eventLoop.makeSucceededFuture(data)
        }
    }

    func template<P: PassKitPass>(for: P, db: Database) -> EventLoopFuture<URL> {
        let url = URL(fileURLWithPath: "/Users/scott/bella/pass", isDirectory: true)
        return db.eventLoop.makeSucceededFuture(url)
    }

    var sslSigningFilesDirectory = URL(fileURLWithPath: "/Users/scott/bella/sign", isDirectory: true)

    var pemPrivateKeyPassword: String? = "12345"
}
