import Fluent
import FluentPostgresDriver
import PostgresKit
import Vapor
import PassKit

// Called before your application initializes.
func configure(_ app: Application) throws {
    let configuration = PostgresConfiguration(hostname: "localhost",
                                              port: 5433,
                                              username: "bella",
                                              password: "2GQeMnL9irnHNp",
                                              database: "bella")
    app.databases.use(.postgres(configuration: configuration), as: .psql)
    app.databases.middleware.use(PassDataMiddleware(app: app), on: .psql)
    
    app.server.configuration.hostname = "0.0.0.0"

    PassKit.register(migrations: app.migrations)
    app.migrations.add(PassData())

    try routes(app)
}
