//
//  PushAuthMiddleware.swift
//  
//
//  Created by Scott Grosch on 12/21/19.
//

import Vapor

struct PushAuthMiddleware: Middleware {
    func respond(to request: Request, chainingTo next: Responder) -> EventLoopFuture<Response> {
        let auth = request.headers["Authorization"]
        guard auth.first == "Bearer 49C09E7D-5FF0-4EAA-95A4-BD8734B08021" else {
            return request.eventLoop.makeFailedFuture(Abort(.unauthorized))
        }

        return next.respond(to: request)
    }
}
