//
//  File.swift
//  
//
//  Created by Scott Grosch on 1/3/20.
//

import Vapor
import Fluent
import PassKit

struct PassDataMiddleware: ModelMiddleware {
    private unowned let app: Application

    init(app: Application) {
        self.app = app
    }

    func update(model: PassData, on db: Database, next: AnyModelResponder) -> EventLoopFuture<Void> {
        next.update(model, on: db).flatMap {
            PassKit.sendPushNotifications(for: model.$pass, on: db, app: self.app)
        }
    }
}

