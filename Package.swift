// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "Bella",
    platforms: [
        .macOS(.v10_15), .iOS(.v13)
    ],
    products: [
        .executable(name: "Run", targets: ["Run"]),
        .library(name: "Bella", targets: ["App"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "4.0.0-beta"),
        .package(url: "https://github.com/vapor/fluent.git", from: "4.0.0-beta"),
        .package(url: "https://github.com/vapor/fluent-postgres-driver.git", from: "2.0.0-beta"),
        //.package(url: "https://github.com/vapor-community/PassKit", .branch("future"))
        .package(url: "https://github.com/vapor-community/PassKit", from: "0.0.4")
    ],
    targets: [
        .target(name: "App", dependencies: ["Fluent", "FluentPostgresDriver", "Vapor", "PassKit"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)


